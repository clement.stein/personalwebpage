Highcharts.chart('english-container', {

    chart: {
        polar: true,
        type: 'area'
    },

    title: {
        useHTML: true,
        text: "<img src='img/flags/c_uk.png' height='30' width='30'/> English (C1 level) <img src='img/flags/c_uk.png' height='30' width='30'/>"
    },

    pane: {
        size: '80%'
    },

    xAxis: {
        categories: ['Speaking', 'Writing', 'Reading', 'Communication',
                'Technical'],
        tickmarkPlacement: 'on',
        lineWidth: 0
    },

    yAxis: {
        gridLineInterpolation: 'polygon',
        lineWidth: 0,
        min: 0,
        max: 100
    },

    tooltip: {
        shared: true,
        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}%</b><br/>'
    },

    credits: {
      enabled: false
    },

    series: [{
        name: 'English',
        color: '#9ae2d3',
        data: [80, 90, 90, 90, 80],
        fillOpacity: 0.2,
        pointPlacement: 'on',
        showInLegend: false
    }]

});