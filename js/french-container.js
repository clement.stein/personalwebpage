Highcharts.chart('french-container', {

    chart: {
        polar: true,
        type: 'area'
    },

    title: {
        useHTML: true,
        text: "<img src='img/flags/c_france.png' height='30' width='30'/> French (mother tongue) <img src='img/flags/c_france.png' height='30' width='30'/>"
    },

    pane: {
        size: '80%'
    },

    xAxis: {
        categories: ['Speaking', 'Writing', 'Reading', 'Communication',
                'Technical'],
        tickmarkPlacement: 'on',
        lineWidth: 0
    },

    yAxis: {
        gridLineInterpolation: 'polygon',
        lineWidth: 0,
        min: 0,
        max: 100
    },

    tooltip: {
        shared: true,
        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}%</b><br/>'
    },

    credits: {
      enabled: false
    },

    series: [{
        name: 'French',
        color: '#9ae2d3',
        data: [100, 100, 100, 100, 100],
        fillOpacity: 0.2,
        pointPlacement: 'on',
        showInLegend: false
    }]

});